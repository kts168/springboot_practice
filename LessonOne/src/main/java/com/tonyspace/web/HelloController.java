package com.tonyspace.web;

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
/**
 * @author: dell
 * @Date: 11:35 2018/5/22
 * @Description:
 */
@RestController
public class HelloController {

    @RequestMapping("/hello")
    public String index() {
        return  "Hello World";
    }
}
