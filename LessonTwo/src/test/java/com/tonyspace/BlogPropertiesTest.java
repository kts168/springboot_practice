package com.tonyspace;

import com.tonyspace.service.BlogProperties;
import org.junit.Assert;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;

/**
 * 第二节 属性读取
 * @author: dell
 * @Date: 18:43 2018/5/23
 * @Description:
 */
public class BlogPropertiesTest extends ApplicationTests {

    @Autowired
    private BlogProperties blogProperties;


    @Test
    public void readProperties() {
        log.info("=========="+blogProperties.getName()+"==============" );
        Assert.assertEquals("我的Springboot之路",blogProperties.getName());
        Assert.assertEquals("Springboot属性读取",blogProperties.getTitle());
        Assert.assertEquals("《我的Springboot之路》持之以恒《Springboot属性读取》",blogProperties.getDesc());

        log.info("整型随机字串:"+blogProperties.getRandomStr());
        log.info("随机数Integer:"+blogProperties.getNumber());
        log.info("随机数Long:"+blogProperties.getBignumber());
        log.info("10以内的随机数:"+blogProperties.getScope1());
        log.info("10-20随机数:"+blogProperties.getScope2());
        log.info("=========="+blogProperties.getName()+"==============" );
    }

}
