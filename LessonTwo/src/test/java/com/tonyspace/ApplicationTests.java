package com.tonyspace;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

/**
 *	第一节：属性读取与多环境配置
 *	第二节：构建RESTFull api接口，单元测试
 *	第三节：集成 Swagger2 restapi接口文档
 *  第四节: thymeleaf 整合模板引擎使用
 *
 *
 *
 *
 * @SpringBootTest 默认配置就是：webEnvironment = SpringBootTest.WebEnvironment.MOCK
 * SpringRunner.class 继承了SpringJUnit4ClassRunner
 */
@RunWith(SpringRunner.class)
@SpringBootTest
public class ApplicationTests {

	public static final Log log = LogFactory.getLog(ApplicationTests.class);

}
