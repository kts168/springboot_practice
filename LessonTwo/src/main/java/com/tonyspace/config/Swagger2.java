package com.tonyspace.config;

import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.servlet.config.annotation.EnableWebMvc;
import springfox.documentation.builders.ApiInfoBuilder;
import springfox.documentation.builders.PathSelectors;
import springfox.documentation.builders.RequestHandlerSelectors;
import springfox.documentation.service.ApiInfo;
import springfox.documentation.service.Contact;
import springfox.documentation.spi.DocumentationType;
import springfox.documentation.spring.web.plugins.Docket;
import springfox.documentation.swagger2.annotations.EnableSwagger2;

/**
 * @author: dell
 * @Date: 19:11 2018/5/23
 * @Description:
 */
@EnableSwagger2
@Configuration
public class Swagger2 {

    @Bean
    public Docket creatRestAPi(){
        return  new Docket(DocumentationType.SWAGGER_2)
                .apiInfo(apiInfo())
                .select()
                .apis(RequestHandlerSelectors.basePackage("com.tonyspace"))
                .paths(PathSelectors.any())
                .build();
    }

    private ApiInfo apiInfo(){
        return new ApiInfoBuilder()
                .title("Spring boot 使用swagger2 构建 REST APIs")
                .description("欢迎关注我们")
                .contact(new Contact("tonyspace","https://my.oschina.net/u/263768/blog","346172831@qq.com"))
                .version("version1.0")
                .build();
    }
}
