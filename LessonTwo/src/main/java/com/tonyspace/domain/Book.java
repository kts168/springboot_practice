package com.tonyspace.domain;

/**
 * @author: dell
 * @Date: 12:46 2018/5/24
 * @Description:
 */
public class Book {

    private String title;
    private Integer cost;
    private String publisher;

    public Book(String title,Integer cost, String publisher){
        this.title = title;
        this.cost = cost;
        this.publisher = publisher;
    }


    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public Integer getCost() {
        return cost;
    }

    public void setCost(Integer cost) {
        this.cost = cost;
    }

    public String getPublisher() {
        return publisher;
    }

    public void setPublisher(String publisher) {
        this.publisher = publisher;
    }
}
