package com.tonyspace.service;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

/**
 * @author: dell
 * @Date: 9:22 2018/5/23
 * @Description:
 */
@Component
public class BlogProperties {

    @Value("${com.tonyspace.blog.name}")
    private String name;
    @Value("${com.tonyspace.blog.title}")
    private String title;
    @Value("${com.tonyspace.blog.desc}")
    private String desc;
    @Value("${com.tonyspace.blog.randomstr}")
    private String randomStr;
    @Value("${com.tonyspace.blog.number}")
    private Integer number;
    @Value("${com.tonyspace.blog.bignumber}")
    private Long bignumber;
    @Value("${com.tonyspace.blog.scope1}")
    private Integer scope1;
    @Value("${com.tonyspace.blog.scope2}")
    private Integer scope2;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getDesc() {
        return desc;
    }

    public void setDesc(String desc) {
        this.desc = desc;
    }

    public String getRandomStr() {
        return randomStr;
    }

    public void setRandomStr(String randomStr) {
        this.randomStr = randomStr;
    }

    public int getNumber() {
        return number;
    }

    public void setNumber(int number) {
        this.number = number;
    }

    public Long getBignumber() {
        return bignumber;
    }

    public void setBignumber(Long bignumber) {
        this.bignumber = bignumber;
    }

    public int getScope1() {
        return scope1;
    }

    public void setScope1(Integer scope1) {
        this.scope1 = scope1;
    }

    public int getScope2() {
        return scope2;
    }

    public void setScope2(Integer scope2) {
        this.scope2 = scope2;
    }
}
