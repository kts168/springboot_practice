package com.tonyspace.web;

import com.tonyspace.domain.Book;
import com.tonyspace.domain.Person;
import com.tonyspace.domain.User;
import com.tonyspace.exception.MyException;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * 第四节: thymeleaf 整合模板引擎使用
 * @author: dell
 * @Date: 10:20 2018/5/24
 * @Description:
 */
@Api(tags = {"模板引擎thymeleaf整合"})
@Controller
public class IndexController {

    @Autowired
    private Person person;

    @ApiOperation(value = "测试thymeleaf 模板引擎")
    @RequestMapping(value = "/", method = RequestMethod.GET )
    public String index(ModelMap map){
        map.addAttribute("Host","http://localhost:1111/");

        Map user= new HashMap();
        user.put("name", "姓名");
        user.put("age", "年龄");
        user.put("sex", "性别");
        user.put("birthday", "生日");
        user.put("phonenumber", "手机");
        map.addAttribute("userhead", user);

        List userinfo =new ArrayList();
        userinfo.add("周美玲");
        userinfo.add("32");
        userinfo.add("女");
        userinfo.add("1985");
        userinfo.add("19850014665");
        map.addAttribute("userinfo", userinfo);

        List outerList=new ArrayList<>();
        Map innerMap=new HashMap<>();
        for (int i = 0; i < 10; i++) {
            innerMap.put("1", i);
            innerMap.put("1", i++);
            i++;
            outerList.add(innerMap);
        }
        map.addAttribute("listmap", outerList);


        List<Book> bookList = new ArrayList<Book>();
        bookList.add(new Book("Spring Boot",10,"人民出版社"));
        bookList.add(new Book("Thymeleaf",10,"人民出版社"));
        bookList.add(new Book("Freemarker",10,"人民出版社"));
        bookList.add(new Book("Velocity",10,"人民出版社"));

        map.addAttribute("books",bookList);



        List<Person> persons = new ArrayList<Person>();
        Person person1 = new Person();
        person1.setName("钟亮");
        person1.setSex("男");
        person1.setAge("12");
        Person person2 = new Person();
        person2.setName("孙正强");
        person2.setSex("男");
        person2.setAge("13");
        persons.add(person);
        persons.add(person1);
        persons.add(person2);
        map.addAttribute("person", person);
        map.addAttribute("persons", persons);

        return "index";
    }



    @RequestMapping("/hellothymeleaf")
    public String hello(Model map){
        Map user= new HashMap();
        user.put("name", "姓名");
        user.put("age", "年龄");
        user.put("sex", "性别");
        user.put("birthday", "生日");
        user.put("phonenumber", "手机");
        map.addAttribute("userhead", user);

        List userinfo =new ArrayList();
        userinfo.add("周美玲");
        userinfo.add("32");
        userinfo.add("女");
        userinfo.add("1985");
        userinfo.add("19850014665");
        map.addAttribute("userinfo", userinfo);

        List outerList=new ArrayList<>();
        Map innerMap=new HashMap<>();
        for (int i = 0; i < 10; i++) {
            innerMap.put("1", i);
            innerMap.put("1", i++);
            i++;
            outerList.add(innerMap);
        }
        map.addAttribute("listmap", outerList);


        List<Book> bookList = new ArrayList<Book>();
        bookList.add(new Book("Spring Boot",10,"人民出版社"));
        bookList.add(new Book("Thymeleaf",10,"人民出版社"));
        bookList.add(new Book("Freemarker",10,"人民出版社"));
        bookList.add(new Book("Velocity",10,"人民出版社"));

        map.addAttribute("books",bookList);


        return "hellothymeleaf";
    }

    @ApiOperation(value = "统一异常处理，当抛出异常时，返回异常信息到view页面")
    @RequestMapping("/exceptionview")
    public String hello() throws Exception {
        throw new Exception("发生错误");
    }

    @ApiOperation(value = "统一异常处理，当抛出异常时，返回json格式的异常")
    @RequestMapping("/exceptionjson")
    public String json() throws MyException {
        throw new MyException("发生错误2");
    }

}
