package com.tonyspace.web;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

/**
 * @author: dell
 * @Date: 17:02 2018/5/24
 * @Description:
 */
@Controller
public class HelloController {

    @RequestMapping("/login")
    public String login() {
        return "login";
    }

}
