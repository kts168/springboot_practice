package com.tonyspace.constant;

/**
 * @author: dell
 * @Date: 15:59 2018/11/23
 * @Description:
 */
public interface WxService {

    String CODE_CALLBACK_URL ="https://wx.szwantech.com/testWechat/codecallback";

    String CONNECT_OAUTH2_AUTHORIZE_URL = "https://open.weixin.qq.com/connect/oauth2/authorize?appid=%s&redirect_uri=%s&response_type=code&scope=%s&state=%s#wechat_redirect";

    /**
     * 用code换取oauth2的access token
     */
    String OAUTH2_ACCESS_TOKEN_URL = "https://api.weixin.qq.com/sns/oauth2/access_token?appid=%s&secret=%s&code=%s&grant_type=authorization_code";
    //以snsapi_base为scope发起的网页授权，是用来获取进入页面的用户的openid的，并且是静默授权并自动跳转到回调页的。
    //用户感知的就是直接进入了回调页（往往是业务页面）
    String SCOPE_SNSAPI_BASE = "snsapi_base";
    // 以snsapi_userinfo为scope发起的网页授权，是用来获取用户的基本信息的。
    // 但这种授权需要用户手动同意，并且由于用户同意过，所以无须关注，就可在授权后获取该用户的基本信息
    String SCOPE_SNSAPI_USERINFO ="snsapi_userinfo";

    String state="qwert";
}
