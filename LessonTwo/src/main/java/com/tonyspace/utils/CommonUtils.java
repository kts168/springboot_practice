package com.tonyspace.utils;

import com.tonyspace.constant.WxService;

import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;

/**
 * @author: dell
 * @Date: 17:13 2018/11/23
 * @Description:
 */
public class CommonUtils {
    /**
     * 获取微信授权url base
     * @param appId
     * @param redirectUrl
     * @return
     */
    public static String getOautho2AuthorizeUrl(String appId,String redirectUrl){
        String url=null;
        try {
            String redirect_url = null;
            redirect_url = URLEncoder.encode(redirectUrl, "utf-8");
            url = String.format(WxService.CONNECT_OAUTH2_AUTHORIZE_URL,appId,redirect_url,WxService.SCOPE_SNSAPI_BASE,WxService.state);
            return url;
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        }
        return  url;
    }

    /**
     * 获取微信token url
     * @param appId
     * @param secret
     * @param code
     * @return
     */
    public static  String getOauth2AcessTokenUrl(String appId,String secret,String code){
        String url = String.format(WxService.OAUTH2_ACCESS_TOKEN_URL,appId,secret,code);
        return  url;
    }
}
