package com.tonyspace;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 *	第一节：属性读取与多环境配置
 *	第二节：构建RESTFull api接口，单元测试
 *	第三节：集成 Swagger2 restapi接口文档
 *  第四节: thymeleaf 整合模板引擎使用
 *
 *
 */
@SpringBootApplication
public class LessonTwoApplication {

	public static void main(String[] args) {
		SpringApplication.run(LessonTwoApplication.class, args);
	}
}
