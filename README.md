# springboot实践

#### 项目介绍
深入实践springboot:
		
    <一>Lessonone 
        [springboot入门,利用idea快速搭建web项目]
    <二>Lessontwo 
        [第一节：属性读取与多环境配置]
        [第二节：构建RESTFull api接口，单元测试]
        [第三节：集成 Swagger2 restapi接口文档]
        [第四节: thymeleaf 整合模板引擎使用]
	<三>LessonThree
		[第一节]：Mybatis 整合
		[第二节]：Dubbo 整合 

#### 软件架构
软件架构说明


#### 安装教程

1. xxxx
2. xxxx
3. xxxx

#### 使用说明

1. xxxx
2. xxxx
3. xxxx

#### 参与贡献

1. Fork 本项目
2. 新建 Feat_xxx 分支
3. 提交代码
4. 新建 Pull Request


#### 码云特技

1. 使用 Readme\_XXX.md 来支持不同的语言，例如 Readme\_en.md, Readme\_zh.md
2. 码云官方博客 [blog.gitee.com](https://blog.gitee.com)
3. 你可以 [https://gitee.com/explore](https://gitee.com/explore) 这个地址来了解码云上的优秀开源项目
4. [GVP](https://gitee.com/gvp) 全称是码云最有价值开源项目，是码云综合评定出的优秀开源项目
5. 码云官方提供的使用手册 [http://git.mydoc.io/](http://git.mydoc.io/)
6. 码云封面人物是一档用来展示码云会员风采的栏目 [https://gitee.com/gitee-stars/](https://gitee.com/gitee-stars/)