package com.tonyspace.service.impl;

import com.tonyspace.domain.User;
import com.tonyspace.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * @author: dell
 * @Date: 10:48 2018/5/28
 * @Description:
 */
@Component("userService")
public class UserServiceImpl implements UserService {

    @Autowired
    private  UserMapper userMapper;

    @Override
    public Integer createUser(User user) {
        return  userMapper.createUser(user);
    }

    @Override
    public User findUser(String name) {
        return  userMapper.findUser(name);
    }

    @Override
    public List<User> findAllUser() {
        return userMapper.findAllUser();
    }
}
