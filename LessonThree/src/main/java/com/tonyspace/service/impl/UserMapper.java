package com.tonyspace.service.impl;

import com.tonyspace.domain.User;
import org.apache.ibatis.annotations.Insert;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Select;

import java.util.List;

/**
 * @author: dell
 * @Date: 11:13 2018/5/28
 * @Description:
 */
@Mapper
public interface UserMapper {

    @Insert(value = "insert into wt_user(name, age) values (#{name},#{age}) ")
    Integer createUser(User user);

    @Select(value = "select * from wt_user where name= #{name}")
    User findUser(String name);

    @Select(value = "select * from wt_user")
    List<User> findAllUser();
}
