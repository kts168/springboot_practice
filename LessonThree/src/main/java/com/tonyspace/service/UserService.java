package com.tonyspace.service;

import com.tonyspace.domain.User;

import java.util.List;

/**
 * @author: dell
 * @Date: 10:44 2018/5/28
 * @Description:
 */
public interface UserService {
    /**
     * 创建用户
     */
    Integer createUser(User user);

    /**
     * 查找某个用户
     * @param name
     * @return
     */
    User findUser(String name);

    /**
     * 查找所有用户
     * @return
     */
    List<User> findAllUser();
}
