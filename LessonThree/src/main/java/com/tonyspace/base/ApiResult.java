package com.tonyspace.base;

/**
 * @author: dell
 * @Date: 11:30 2018/5/28
 * @Description:
 */
public class ApiResult extends BaseResult {

    public ApiResult(int code, String message, Object data) {
        super(code, message, data);
    }

    public ApiResult(ApiResultConstant apiResultConstant, Object data) {
        super(apiResultConstant.getCode(), apiResultConstant.getMessage(), data);
    }

    public static ApiResult getSuccessObject(String msg) {
        return new ApiResult(ApiResultConstant.ERROR_SUCCESS.getCode(), msg, null);
    }

    public static ApiResult getErrorResult(String msg) {
        return new ApiResult(ApiResultConstant.ERROR_FAIL.getCode(), msg, null);
    }

    public static ApiResult getObjectResult(Object data) {
        return new ApiResult(ApiResultConstant.ERROR_SUCCESS, data);
    }

    public static ApiResult getResult(ApiResultConstant apiResultConstant) {
        return new ApiResult(apiResultConstant, null);
    }

    public static ApiResult getResult(ApiResultConstant apiResultConstant, String msg) {
        return new ApiResult(apiResultConstant.getCode(), msg, null);
    }

    public static ApiResult getSuccessResult() {
        return new ApiResult(ApiResultConstant.ERROR_FAIL.getCode(), ApiResultConstant.ERROR_SUCCESS.getMessage(), null);
    }

}
