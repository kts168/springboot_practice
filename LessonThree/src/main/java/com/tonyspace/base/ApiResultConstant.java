package com.tonyspace.base;

/**
 * @author: dell
 * @Date: 11:30 2018/5/28
 * @Description:api返回结果统一常量
 */
public enum ApiResultConstant {

	// 成功
	ERROR_SUCCESS(1, "ok"),
	// 失败
	ERROR_FAIL(0, "fail"),
	// 未登录
	ERROR_NOT_LOGON(-1, "not login"),
	// 数据库查询出错
	ERROR_DATABASE(-2, "database error"),
	// 无权限操作
	ERROR_NO_ACCESS(-3, "no access"),
	// 无效参数
	ERROR_INVALID_PARAM(-4, "invalid param"),
	// 无效账号或密码
	ERROR_INVALID_UID_OR_PASSWD(-5, "invalid uid or password"),
	// 无效验证码
	ERROR_INVALID_CODE(-6, "invalid code"),
	// 无效token
	ERROR_INVALID_TOKEN(-7, "invalid token");	

    public int code;

    public String message;

    ApiResultConstant(int code, String message) {
        this.code = code;
        this.message = message;
    }

    public int getCode() {
        return code;
    }

    public void setCode(int code) {
        this.code = code;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }
}
