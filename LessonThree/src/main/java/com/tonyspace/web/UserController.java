package com.tonyspace.web;

import com.tonyspace.base.ApiResult;
import com.tonyspace.domain.User;
import com.tonyspace.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * @author: dell
 * @Date: 11:35 2018/5/22
 * @Description:
 */
@RestController
@RequestMapping(value = "/users")
public class UserController {

    @Autowired(required = true)
    @Qualifier(value = "userService")
    private UserService userService;

    @RequestMapping(value = "/" ,method = RequestMethod.GET)
    public ApiResult getUsers() {
        List<User> list = userService.findAllUser();
        return  ApiResult.getObjectResult(list);
    }

    @RequestMapping(value = "/" ,method = RequestMethod.POST)
    public ApiResult createUser(@ModelAttribute User user){
        userService.createUser(user);
        return  ApiResult.getSuccessResult();
    }

}
