package com.tonyspace;

import com.tonyspace.service.UserService;
import com.tonyspace.web.UserController;
import org.apache.ibatis.executor.resultset.DefaultResultSetHandler;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.omg.CORBA.PUBLIC_MEMBER;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.http.MediaType;
import org.springframework.mock.web.MockServletContext;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.RequestBuilder;
import org.springframework.test.web.servlet.ResultHandler;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultHandlers;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;
import org.springframework.test.web.servlet.result.PrintingResultHandler;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;

import static org.hamcrest.Matchers.equalTo;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;


@RunWith(SpringRunner.class)
@SpringBootTest
public class LessonThreeApplicationTests {
	private MockMvc mvc;
	private RequestBuilder request ;

	private  static  final Logger logger = LoggerFactory.getLogger(LessonThreeApplicationTests.class);

	@Autowired
    private WebApplicationContext webApplicationContext;

	@Before
	public void setUp() throws Exception {
        mvc = MockMvcBuilders.webAppContextSetup(webApplicationContext).build();
	}

	@Test
    public void postUser()throws  Exception{
        // 2、post提交一个user
        request = post("/users/")
                .param("id", "1")
                .param("name", "测试大师")
                .param("age", "20")
                .accept(MediaType.APPLICATION_JSON)
                .characterEncoding("utf-8");
        mvc.perform(request).andDo(MockMvcResultHandlers.print())
                            .andExpect(status().isOk())
                            .andExpect(content().string(equalTo("{\"code\":0,\"message\":\"ok\",\"data\":null}")));

    }
	@Test
	public void getUsers() throws  Exception {
        mvc.perform(MockMvcRequestBuilders.post("/users/")
                .accept(MediaType.APPLICATION_JSON)
                .param("name","tony")
                .param("age","11")
                .characterEncoding("utf-8"))
                .andExpect(status().isOk());


		mvc.perform(MockMvcRequestBuilders.get("/users/")
                .accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk()).andDo(new ResultHandler() {
			@Override
			public void handle(MvcResult mvcResult) throws Exception {
				logger.info(mvcResult.getResponse().getContentAsString());
			}
		});

        MvcResult mvcResult = mvc.perform(MockMvcRequestBuilders.get("/users/")
                .accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andDo(MockMvcResultHandlers.print())
                .andReturn();


//        System.out.println(mvcResult.getResponse().getContentAsString());


//        //测试页面中的属性值
//        MvcResult result = mvc.perform(MockMvcRequestBuilders.get("/user/1"))
//                .andExpect(MockMvcResultMatchers.view().name("user/view"))
//                .andExpect(MockMvcResultMatchers.model().attributeExists("user"))
//                .andDo(MockMvcResultHandlers.print())
//                .andReturn();

       // Assert.assertNotNull(result.getModelAndView().getModel().get("user"));

    }

}
