package com.fangyaxun.jwttoken;


import lombok.extern.slf4j.Slf4j;
import org.springframework.boot.ApplicationArguments;
import org.springframework.boot.ApplicationRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.SignatureAlgorithm;
import io.jsonwebtoken.security.Keys;
import java.security.Key;

@Slf4j
@SpringBootApplication
public class JwttokenApplication  implements ApplicationRunner {

	public static void main(String[] args) {
		SpringApplication.run(JwttokenApplication.class, args);
	}

	@Override
	public void run(ApplicationArguments args) throws Exception {
		log.info("--Application Runing---");
		testJwtToken();
	}

	public void testJwtToken(){
		Key key = Keys.secretKeyFor(SignatureAlgorithm.HS256);
        String jws = Jwts.builder().setSubject("Joe").signWith(key).compact();
		Jwts.header();


        log.info("jws {}",jws);
		assert Jwts.parser().setSigningKey(key).parseClaimsJws(jws).getBody().getSubject().equals("JoE");
	}


}
